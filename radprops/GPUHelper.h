/**
 *  \file   GPUHelper.h
 *  \date   August, 2016
 *  \author Babak Goshayeshi
 *
 * Copyright (c) 2014-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifdef ENABLE_CUDA

#ifndef GPUHelper_h
#define GPUHelper_h

#include <radprops/RadPropsConfig.h>

#include <cuda_runtime.h>
#include <cuda.h>
#include <cuda_runtime_api.h>

namespace RadProps {
#define INNER_BLOCK_ONE_DIM  1024
#define NBLOCK(n) (n / INNER_BLOCK_ONE_DIM + 1)
#define NTHREAD(n) ((n > INNER_BLOCK_ONE_DIM) ? INNER_BLOCK_ONE_DIM : n)

//===================================================================
#define GPU_ERROR_CHECK { gpu_error( cudaGetLastError(), __FILE__, __LINE__); }
// GPU_ERROR_CHECK(cudaGetLastError())
inline void gpu_error(cudaError err, const char *file, int line)
{
  if (cudaSuccess != err){
    std::ostringstream msg;
    msg << "GPU failed!\n"
    << "Cuda message : " << cudaGetErrorString(err) << "\n"
    << "\t - " << file << " : " << line << std::endl;
    throw(std::runtime_error(msg.str()));
  }
}

//===================================================================

inline __global__ void gpu_log(double* ans, const double* val, const size_t indepsize)
{
  const size_t idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx > indepsize) return;
  ans[idx] = log(val[idx]);
}

//===================================================================
inline __global__ void gray_gas_gpu_mixture_coeffs(double* result,
                                            const double* coeff,
                                            const double* molefrac,
                                            const size_t indepsize)
{
  
  const size_t idx = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (idx > indepsize) return;
  result [idx] += molefrac[idx] * coeff[idx];
  // it is not required to synchronize!
}

} // namespace RadProps
#endif // GPUHelper_h
#endif // ENABLE_CUDA