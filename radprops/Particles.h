/**
 *  \file   RadiativeSpecies.h
 *  \date   January, 2012
 *  \author Lyubima Simeonova
 *
 * Copyright (c) 2014-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef Particles_h
#define Particles_h

#include <complex>

#include <radprops/RadPropsConfig.h>

class LagrangeInterpolant2D;  // forward declaration
class LagrangeInterpolant3D;  // forward declaration

namespace RadProps {
  
/**
 *  \class ParticleRadCoeffs
 *  \brief holds the spectrally resolved and effective absorption efficiencies multiplied by \f$\pi r^{2}\f$ for particles
 */
class ParticleRadCoeffs
{
public:

  /**
   * @param refIndex the refractive index of the particle.
   * @param ramin  minimium radii in table
   * @param ramax  maximium radii in table
   * @param nRadii total rad
   * @param interpOrder the order for interpolants (default is first order - linear interpolation).
   */
  ParticleRadCoeffs( const std::complex<double> refIndex,
                     const double ramin,
                     const double ramax,
                     const int nRadii,
                     const unsigned interpOrder=1 );

  ~ParticleRadCoeffs();

  /**
   * @return \f$\frac{\kappa_\lambda}{N_T}\f$ - the spectral absorption
   *  coefficient divided by the particle number density, Units: (m<sup>2</sup>)/particle
   *
   * @param wavelength (m)
   * @param r  particle radius (m)
   *
   * The particle spectral absorption coefficient is given as
   * \f[\kappa_\lambda = \pi r^2 N_T Q_{abs}\f]
   * This method returns
   * \f$\frac{\kappa_\lambda}{N_T}\f$
   * You should multiply by the particle number density, \f$N_T\f$ to obtain
   * the actual particle absorption coefficient.
   */
  double abs_spectral_coeff( const double wavelength,
                             const double r ) const;


  /**
   * @return \f$\frac{\sigma_{s\lambda}}{N_T}\f$ - the scattering coefficient
   *  divided by the particle number density, in units of m<sup>2</sup>/particle.
   *
   * @param wavelength (m)
   * @param r  radius (m)
   *
   * The spectral scattering coefficient is given by
   * \f[ \sigma_{s\lambda} = \pi r^2 N_T Q_{sca}\f]
   * where \f$Q_{sca}\f$ is the scattering efficiency and \f$N_T\f$ is the
   * particle number density.
   */
  double scattering_spectral_coeff( const double wavelength,
                                    const double r ) const;
  /**
   * @return \f$\frac{\kappa_{P}}{N_T}\f$ - the Planck-mean absorption
   *  coefficient divided by the particle number density, (m<sup>2</sup>)/particle.
   *
   * @param r  radius (m)
   * @param T  temperature (K)
   *
   * The particle Planck-mean absorption coefficient is given as
   * \f[\kappa_P = \pi r^2 N_T Q_{abs,P}\f]
   * This method returns
   * \f$\frac{\kappa_P}{N_T}\f$
   * You should multiply by the particle number density, \f$N_T\f$ to obtain
   * the actual particle absorption coefficient.
   */
  double planck_abs_coeff( const double r,
                           const double T ) const;

  /**
   * @return \f$\frac{\sigma_{P}}{N_T}\f$ - the Planck-mean scattering
   *  coefficient divided by the particle number density, (m<sup>2</sup>)/particle.
   *
   * @param r  radius (m)
   * @param T  temperature (K)
   *
   * The particle Planck-mean scattering coefficient is given as
   * \f[\sigma_P = \pi r^2 N_T Q_{sca,P}\f]
   * This method returns
   * \f$\frac{\sigma_P}{N_T}\f$
   * You should multiply by the particle number density, \f$N_T\f$ to obtain
   * the actual particle scattering coefficient.
   */
  double planck_sca_coeff( const double r,
                           const double T) const;

  /**
   * @return \f$\frac{\kappa_{R}}{N_T}\f$ - the Rosseland-mean absorption
   *  coefficient divided by the particle number density, (m<sup>2</sup>)/particle.
   *
   * @param r  radius (m)
   * @param T  temperature (K)
   *
   * The particle Rosseland-mean absorption coefficient is given as
   * \f[\kappa_R = \pi r^2 N_T Q_{abs,R}\f]
   * This method returns
   * \f$\frac{\kappa_P}{N_T}\f$
   * You should multiply by the particle number density, \f$N_T\f$ to obtain
   * the actual particle absorption coefficient.
   */
  double ross_abs_coeff( const double r,
                         const double T ) const;

  /**
   * @return \f$\frac{\sigma_{R}}{N_T}\f$ - the Rosseland-mean scattering
   *  coefficient divided by the particle number density, (m<sup>2</sup>)/particle.
   *
   * @param r  radius (m)
   * @param T  temperature (K)
   *
   * The particle Rosseland-mean scattering coefficient is given as
   * \f[\sigma_R = \pi r^2 N_T Q_{sca,R}\f]
   * This method returns
   * \f$\frac{\sigma_R}{N_T}\f$
   * You should multiply by the particle number density, \f$N_T\f$ to obtain
   * the actual particle scattering coefficient.
   */
  double ross_sca_coeff( const double r,
                         const double T ) const;
  
# ifdef ENABLE_CUDA
  void gpu_abs_spectral_coeff( double* result, const double* const wavelength, const double* const r, const size_t indepsize) const;
  
  void gpu_scattering_spectral_coeff( double* result, const double* const wavelength, const double* const r, const size_t indepsize) const;
  
  void gpu_planck_abs_coeff( double* result, const double* const r, const double* const T, const size_t indepsize) const;
  
  void gpu_planck_sca_coeff( double* result, const double* const r, const double* const T, const size_t indepsize) const;
  
  void gpu_ross_abs_coeff( double* result, const double* const r, const double* const T, const size_t indepsize) const;
  
  void gpu_ross_sca_coeff( double* result, const double* const r, const double* const T, const size_t indepsize) const;
  
# endif
private:

  LagrangeInterpolant2D* interp2AbsSpectralCoeff_;
  LagrangeInterpolant2D* interp2ScaSpectralCoeff_;

  LagrangeInterpolant2D* interp2AbsPlanckCoeff_;
  LagrangeInterpolant2D* interp2ScaPlanckCoeff_;
  LagrangeInterpolant2D* interp2AbsRossCoeff_;
  LagrangeInterpolant2D* interp2ScaRossCoeff_;

};


/**
 * \class ParticleRadCoeffs3D
 * \author Derek Harris
 * \date  December, 2014
 *
 * \brief Extends the ParticleRadCoeffs class to allow a range of refractive indices to be considered.
 */
class ParticleRadCoeffs3D
{
public:

  /**
   * @param refIndexLo the lower bound index of the particle (real component).
   * @param refIndexHi the higher bound index of the particle (real component).
   * @param nRes number of grid points in the complex index of refraction space.
   * @param ramin  minimium radii in table
   * @param ramax  maximium radii in table
   * @param nRadii total rad
   * @param interpOrder the order for interpolants (default is first order - linear interpolation).
   */
  ParticleRadCoeffs3D( const std::complex<double> refIndexLo,
                       const std::complex<double> refIndexHi,
                       const int nRes,
                       const double ramin,
                       const double ramax,
                       const int nRadii, 
                       const unsigned interpOrder=1 );

  ~ParticleRadCoeffs3D();

  /**
   * @return \f$\frac{\kappa_\lambda}{N_T}\f$ - the spectral absorption
   *  coefficient divided by the particle number density, Units: (m<sup>2</sup>)/particle
   *
   * @param wavelength (m)
   * @param r  particle radius (m)
   * @param IRreal real part of the index of refraction
   *
   * The particle spectral absorption coefficient is given as
   * \f[\kappa_\lambda = \pi r^2 N_T Q_{abs}\f]
   * This method returns
   * \f$\frac{\kappa_\lambda}{N_T}\f$
   * You should multiply by the particle number density, \f$N_T\f$ to obtain
   * the actual particle absorption coefficient.
   */
  double abs_spectral_coeff( const double wavelength,
                             const double r ,
                             const double IRreal ) const;


  /**
   * @return \f$\frac{\sigma_{s\lambda}}{N_T}\f$ - the scattering coefficient
   *  divided by the particle number density, in units of m<sup>2</sup>/particle.
   *
   * @param wavelength (m)
   * @param r  radius (m)
   * @param IRreal real part of the index of refraction
   *
   * The spectral scattering coefficient is given by
   * \f[ \sigma_{s\lambda} = \pi r^2 N_T Q_{sca}\f]
   * where \f$Q_{sca}\f$ is the scattering efficiency and \f$N_T\f$ is the
   * particle number density.
   */
  double scattering_spectral_coeff( const double wavelength,
                                    const double r ,
                                    const double IRreal ) const;
  /**
   * @return \f$\frac{\kappa_{P}}{N_T}\f$ - the Planck-mean absorption
   *  coefficient divided by the particle number density, (m<sup>2</sup>)/particle.
   *
   * @param r  radius (m)
   * @param T  temperature (K)
   * @param IRreal real part of the index of refraction
   *
   * The particle Planck-mean absorption coefficient is given as
   * \f[\kappa_P = \pi r^2 N_T Q_{abs,P}\f]
   * This method returns
   * \f$\frac{\kappa_P}{N_T}\f$
   * You should multiply by the particle number density, \f$N_T\f$ to obtain
   * the actual particle absorption coefficient.
   */
  double planck_abs_coeff( const double r,
                           const double T,
                           const double IRreal ) const;

  /**
   * @return \f$\frac{\sigma_{P}}{N_T}\f$ - the Planck-mean scattering
   *  coefficient divided by the particle number density, (m<sup>2</sup>)/particle.
   *
   * @param r  radius (m)
   * @param temperature  temperature (K)
   * @param IRreal real part of the index of refraction
   *
   * The particle Planck-mean scattering coefficient is given as
   * \f[\sigma_P = \pi r^2 N_T Q_{sca,P}\f]
   * This method returns
   * \f$\frac{\sigma_P}{N_T}\f$
   * You should multiply by the particle number density, \f$N_T\f$ to obtain
   * the actual particle scattering coefficient.
   */
  double planck_sca_coeff( const double r,
                           const double temperature,
                           const double IRreal ) const;

  /**
   * @return \f$\frac{\kappa_{R}}{N_T}\f$ - the Rosseland-mean absorption
   *  coefficient divided by the particle number density, (m<sup>2</sup>)/particle.
   *
   * @param r  radius (m)
   * @param temperature  temperature (K)
   * @param IRreal real part of the index of refraction
   *
   * The particle Rosseland-mean absorption coefficient is given as
   * \f[\kappa_R = \pi r^2 N_T Q_{abs,R}\f]
   * This method returns
   * \f$\frac{\kappa_P}{N_T}\f$
   * You should multiply by the particle number density, \f$N_T\f$ to obtain
   * the actual particle absorption coefficient.
   */
  double ross_abs_coeff( const double r,
                         const double temperature,
                         const double IRreal ) const;

  /**
   * @return \f$\frac{\sigma_{R}}{N_T}\f$ - the Rosseland-mean scattering
   *  coefficient divided by the particle number density, (m<sup>2</sup>)/particle.
   *
   * @param r  radius (m)
   * @param temperature temperature (K)
   * @param IRreal real part of the index of refraction
   *
   * The particle Rosseland-mean scattering coefficient is given as
   * \f[\sigma_R = \pi r^2 N_T Q_{sca,R}\f]
   * This method returns
   * \f$\frac{\sigma_R}{N_T}\f$
   * You should multiply by the particle number density, \f$N_T\f$ to obtain
   * the actual particle scattering coefficient.
   */
  double ross_sca_coeff( const double r,
                         const double temperature,
                         const double IRreal ) const;
  
# ifdef ENABLE_CUDA
  void gpu_abs_spectral_coeff( double* result, const double* const wavelength, const double* const r, const double* IRreal, const size_t indepsize) const;
  
  void gpu_scattering_spectral_coeff( double* result, const double* const wavelength, const double* const r, const double* IRreal, const size_t indepsize) const;
  
  void gpu_planck_abs_coeff( double* result, const double* const r, const double* const T, const double* IRreal, const size_t indepsize) const;
  
  void gpu_planck_sca_coeff( double* result, const double* const r, const double* const T, const double* IRreal, const size_t indepsize) const;
  
  void gpu_ross_abs_coeff( double* result, const double* const r, const double* const T, const double* IRreal, const size_t indepsize) const;
  
  void gpu_ross_sca_coeff( double* result, const double* const r, const double* const T, const double* IRreal, const size_t indepsize) const;
  
# endif


private:

  LagrangeInterpolant3D* interp2AbsSpectralCoeff_;
  LagrangeInterpolant3D* interp2ScaSpectralCoeff_;

  LagrangeInterpolant3D* interp2AbsPlanckCoeff_;
  LagrangeInterpolant3D* interp2ScaPlanckCoeff_;
  LagrangeInterpolant3D* interp2AbsRossCoeff_;
  LagrangeInterpolant3D* interp2ScaRossCoeff_;

};

/**
 * \fn double soot_abs_coeff( const double lambda, const double volFrac, const std::complex<double> refIndex );
 *
 * @return the absorption coefficient for soot given by
 * \f[ \kappa_\lambda = \frac{36\pi n k f_v}{\lambda((n^2-k^2+2)^2+4n^2k^2)} \f]
 * Units: 1/m
 *
 * @param lambda the wavelength (m)
 * @param volFrac volume fraction of soot
 * @param refIndex the index of refraction, \f$ m=n-ik \f$
 */
double
soot_abs_coeff( const double lambda,
                const double volFrac,
                const std::complex<double> refIndex );
  
} // namespace RadProps

#endif // Particles_h



