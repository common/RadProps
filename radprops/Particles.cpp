/*
 * Copyright (c) 2014-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <radprops/Particles.h>

#include <vector>
#include <cmath>
#include <iostream>
#include <stdexcept>

#include <tabprops/LagrangeInterpolant.h>

#ifdef ENABLE_CUDA

#include <radprops/GPUHelper.h>

#endif

namespace RadProps {

using namespace std;

//==============================================================================

/**
 * \class Mie
 * \brief Orion Sky Lawlor, olawlor@acm.org, 9/23/2000
 * Computes so-called "Mie scattering" for a homogenous
 * sphere of arbitrary size illuminated by coherent
 * harmonic radiation.
 */
class Mie
{
public:
  Mie( const double xt, const std::complex<double>& mt );
  ~Mie();

  void calcS(double theta);

  double getQext() const{ return qext; }
  double getQsca() const{ return qsca; }
  double getQabs() const{ return qext-qsca; }
  std::complex<double> getS1() const{ return s1; }
  std::complex<double> getS2() const{ return s2; }

  int getNmax() const{return n;}
  std::complex<double> getM() const{ return m; }
  double getX() const{ return x; }

private:
  const std::complex<double> m;
  const double x;
  int n;
  std::complex<double> mx;
  int Nstar;

  std::complex<double> *an;
  std::complex<double> *bn;
  std::complex<double> *rn;
  std::complex<double> ccotmx;
  double *chi;
  double *psi;
  double *pi;
  double qext;
  double qsca;
  double qabs;
  std::complex<double> s1;
  std::complex<double> s2;

  std::complex<double> Cot(std::complex<double> mx);

  void calcPi(double mu);
  double Tau(double mu,int n);
  void calcChi(double x,int n);
  void calcPsi(double x,int n);
  std::complex<double> Zeta(double x,int n);

  void calcR();
  void calcAnBn();

  void calcSi(double theta);
  void calcQscaQext();
};


Mie::Mie( const double xt, const complex<double>& mt )
: m(mt), x(xt)
{
  mx=m*x;
  Nstar=60000;
  n=x+4.0*pow(x,1.0/3.0)+2.0+10; // when to cut of the summations (term taken from Hong Du)
  an=new complex<double>[n];
  bn=new complex<double>[n];
  rn=new complex<double>[Nstar+1];
  chi=new double[n+1];
  psi=new double[n+1];
  pi=new double[n+1];
  calcChi(x,n);
  calcPsi(x,n);
  ccotmx=conj(Cot(conj(mx)));
  calcR();
  calcAnBn();
  calcQscaQext();
}

Mie::~Mie(){
  delete [] an;
  delete [] bn;
  delete [] rn;
  delete [] chi;
  delete [] psi;
  delete [] pi;
}


void Mie::calcS(double theta){
  calcSi(theta);
}

void Mie::calcPi(double mu){
  double s=0.0,t=0.0;

  pi[0]=0.0;
  pi[1]=1.0;

  for(int i=1;i<n;i++){
    s=mu*pi[i];
    t=s-pi[i-1];
    pi[i+1]=s+t+t/(double)i;
  }
}

double Mie::Tau(double mu,int n){
  if(n==0){
    return 0.0;
  }else{
    return n*(mu*pi[n]-pi[n-1])-pi[n-1];
  }
}

void Mie::calcChi(double x,int n){
  chi[0]=cos(x);
  chi[1]=chi[0]/x+sin(x);

  for(int i=1;i<n;i++){
    chi[i+1]=(2.0*i+1.0)*chi[i]/x-chi[i-1];
  }
}

void Mie::calcPsi(double x,int n){
  psi[0]=sin(x);
  psi[1]=psi[0]/x-cos(x);

  for(int i=1;i<n;i++){
    psi[i+1]=(2.0*i+1.0)*psi[i]/x-psi[i-1];
  }
}

complex<double> Mie::Zeta(double x,int n){
  return complex<double>(psi[n],chi[n]);
}

complex<double> Mie::Cot(complex<double> mx){
  const double ctan=tan(real(mx));
  const double cexp=exp(-2.0*imag(mx));

  const complex<double> num   = complex<double>(0,1)+ctan-cexp*ctan+complex<double>(0,1.0)*cexp;
  const complex<double> denom = -(double)1.0+complex<double>(0,1.0)*ctan+complex<double>(0,1.0)*cexp*ctan+cexp;

  const double realnum=real(num);
  const double imagnum=imag(num);
  const double realdenom=real(denom);
  const double imagdenom=imag(denom);

  const double div=(realdenom*realdenom+imagdenom*imagdenom);

  const double realcot=((realnum*realdenom+imagnum*imagdenom)/div);
  const double imagcot=((imagnum*realdenom-realnum*imagdenom)/div);

  return complex<double>(realcot,imagcot);
}

void Mie::calcR(){ //downward recurrence
  rn[Nstar-1]=(double)(2.0*Nstar+1.0)/mx;

  for(int i=Nstar-1;i>=1;i--){
    rn[i-1]=(2.0*i+1.0)/mx-1.0/rn[i];
  }
}

void Mie::calcAnBn(){
  complex<double> rf;

  for(int nc=1;nc<=n;nc++){
    rf=(rn[nc-1]/m+(double)nc*(1.0-1.0/(m*m))/x);

    an[nc-1]=(rf*psi[nc]-psi[nc-1])/(rf*Zeta(x,nc)-Zeta(x,nc-1));
    bn[nc-1]=((complex<double>)rn[nc-1]*(complex<double>)m*psi[nc]-psi[nc-1])/((complex<double>)rn[nc-1]*(complex<double>)m*Zeta(x,nc)-Zeta(x,nc-1));
  }
}

void Mie::calcSi(double theta){
  double mu=cos(theta);
  double tau;
  double fn;

  s1=complex<double>(0,0);
  s2=complex<double>(0,0);

  calcPi(mu);

  for(int i=1;i<=n;i++){
    tau=Tau(mu,i);
    fn=(double)(2.0*i+1.0)/(double)(i*(i+1.0));
    s1=s1+fn*(an[i-1]*pi[i]+bn[i-1]*tau);
    s2=s2+fn*(an[i-1]*tau+bn[i-1]*pi[i]);
  }
}

void Mie::calcQscaQext(){
  qsca=0;
  qext=0;

  for(int i=1;i<=n;i++){
    qext=qext+(double)(2.0*i+1.0)*(an[i-1].real()+bn[i-1].real());
    qsca=qsca+(double)(2.0*i+1.0)*(pow(abs(an[i-1]),2)+pow(abs(bn[i-1]),2));
  }

  qext=qext*2.0/(x*x);
  qsca=qsca*2.0/(x*x);
}

//==============================================================================

double
soot_abs_coeff( const double lambda,
                const double volFrac,
                const complex<double> refIndex )
{
  // jcs we could significantly improve the efficiency here by calculating some temporaries and using those...
  return 36*M_PI*refIndex.real()*abs(refIndex.imag())*volFrac / (
   ( (refIndex.real()*refIndex.real()-abs(refIndex.imag())*abs(refIndex.imag())+2 ) *
     (refIndex.real()*refIndex.real()-abs(refIndex.imag())*abs(refIndex.imag())+2 ) + 4*
     refIndex.real()*abs(refIndex.imag())*refIndex.real()*abs(refIndex.imag()) ) * lambda );
}


/**
 * @param absSpectralEff - vector of spectral absorption efficiencies multiplied by \f$\pi r^{2}\f$. Units: (m<sup>2</sup>)/particle
 * @param scaSpectralEff - vector of spectral scattering efficiencies multiplied by \f$\pi r^{2}\f$. Units: (m<sup>2</sup>)/particle
 * @param lambdas
 * @param raV - vector of radii (m)
 * @param ramin - smallest particle radius to be tabulated (m)
 * @param ramax - largest particle radius to be tabulated (m)
 * @param nRadii - nRadii (m)
 * @param refIndex - refIndex  (-)
 */
void
spectral_effs( std::vector<double>& absSpectralEff,
               std::vector<double>& scaSpectralEff,
               std::vector<double>& lambdas,
               std::vector<double>& raV,
               const double ramin,
               const double ramax,
               const int nRadii, //number of radii to be considered
               const complex<double> refIndex )
{
  const double wl_max = 12.5e-6;  // (m)
  const double wl_min = 1e-7;     // (m)
  const int N=100;
  lambdas.resize(N);

  const double log_wl_max = log(wl_max);
  const double log_wl_min = log(wl_min);
  const double log_wl_step = (log_wl_max-log_wl_min)/N;

  const int NR = N*nRadii;
  absSpectralEff.resize(NR);
  scaSpectralEff.resize(NR);
  raV.resize(nRadii);


  const double log_ramax = log(ramax);
  const double log_ramin = log(ramin);

  const double log_ra_step = (log_ramax-log_ramin)/nRadii;

  int coeff = 0;
  for( int r=0; r<nRadii; ++r ){
    raV[r]=log_ramin+r*log_ra_step;
    double x;
    for( int i=0; i<N; ++i ){
      lambdas[i]= log_wl_min+i*log_wl_step;
      x=2*M_PI*exp(raV[r])/exp(lambdas[i]);
      Mie mie2(x,refIndex );

      double qabs=mie2.getQabs();
      double qsca=mie2.getQsca();

      absSpectralEff[coeff] = qabs;
      scaSpectralEff[coeff] = qsca;

      coeff+=1;
    }
  }
}


/**
 * @brief Calculates the spectral absorption and scattering efficiencies multiplied by \f$\pi r^{2}\f$ using Mie theory
 * and tabulates them for a vector of radii and wavelengths
 * \f$\kappa_\lambda = \pi r^2 Q_{abs}\f$. Units: 1/m
 * \f$\sigma_{s\lambda} = \pi r^2 Q_{sca}\f$. Units: 1/m
 * @param absSpectralCoeff - vector of spectral absorption efficiencies multiplied by \f$\pi r^{2}\f$. Units: (m<sup>2</sup>)/particle
 * @param scaSpectralCoeff - vector of spectral scattering efficiencies multiplied by \f$\pi r^{2}\f$. Units: (m<sup>2</sup>)/particle
 * @param absSpectralEff - vector of spectral absorption efficiencies multiplied by \f$\pi r^{2}\f$. Units: (m<sup>2</sup>)/particle
 * @param scaSpectralEff - vector of spectral scattering efficiencies multiplied by \f$\pi r^{2}\f$. Units: (m<sup>2</sup>)/particle
 * @param lambdas - vector of wavelengths (m)
 * @param raV - vector of radii (m)
 */
void
spectral_coeff( std::vector<double>& absSpectralCoeff,
                std::vector<double>& scaSpectralCoeff,
                const std::vector<double>& absSpectralEff,
                const std::vector<double>& scaSpectralEff,
                const std::vector<double>& lambdas,
                const std::vector<double>& raV )
{
  const int nL = lambdas.size();
  const int nR= raV.size(); //number of radii to be considered
  const int nRL = nR*nL;
  absSpectralCoeff.resize(nRL);
  scaSpectralCoeff.resize(nRL);

  for (int r=0;r<nR;r++){
    for (int i=0;i<nL;i++){
      // exp(r) because we have done a log-transform on the radii previously.
      absSpectralCoeff[r*nL+i] = M_PI*exp(raV[r])*exp(raV[r])*absSpectralEff[r*nL+i];
      scaSpectralCoeff[r*nL+i] = M_PI*exp(raV[r])*exp(raV[r])*scaSpectralEff[r*nL+i];
#     ifdef RADPROPS_DEBUG_INFO
      cout  << exp(lambdas[i]) << " " << exp(raV[r]) << " " << absSpectralCoeff[r*nL+i] << " " << scaSpectralCoeff[r*nL+i] << endl;
#     endif // RADPROPS_DEBUG_INFO
    }
  }
}


/**
 * @brief Calculates the the Planck-mean and Rosseland-mean absorption and scattering efficiencies multiplied by \f$\pi r^{2}\f$
 * and tabulates them for a vector of radii and temperatures
 * \f$\kappa_P = \pi r^2 Q_{abs P}\f$. Units: 1/m
 * \f$\sigma_{s P} = \pi r^2 Q_{sca P}\f$. Units: 1/m
 * \f$\kappa_R = \pi r^2 Q_{abs R}\f$. Units: 1/m
 * \f$\sigma_{s R} = \pi r^2 Q_{sca R}\f$. Units: 1/m
 * \f$\bar{Q_{P}}=\frac{15}{\pi^{4}}\int_{0}^{\infty}Q_{\xi}\xi^{3}(e^{\xi}-1)^{-1}\: d\xi\f$
 * \f$\frac{1}{\bar{Q_{R}}}=\frac{15}{\pi^{4}}\int_{0}^{\infty}\frac{1}{Q_{\xi}}\xi^{4}e^{\xi}(e^{\xi}-1)^{-2}\: d\xi\f$
 * where \f$\xi=C_{2}/{\lambda T}\f$ and \f$C_{2}=0.0144\f$ mK is Planck's second radiation constant,
 * and \f$Q_{\xi}\f$ is the respective efficiency calculated at the appropriate wavelength and radius
 * @param absPlanckCoeff - vector of Planck-mean absorption efficiencies multiplied by \f$\pi r^{2}\f$. Units: (m<sup>2</sup>)/particle
 * @param scaPlanckCoeff - vector of Planck-mean scattering efficiencies multiplied by \f$\pi r^{2}\f$. Units: (m<sup>2</sup>)/particle
 * @param absRossCoeff - vector of Rosseland-mean absorption efficiencies multiplied by \f$\pi r^{2}\f$. Units: (m<sup>2</sup>)/particle
 * @param scaRossCoeff - vector of Rosseland-mean scattering efficiencies multiplied by \f$\pi r^{2}\f$. Units: (m<sup>2</sup>)/particle
 * @param absSpectralEff - vector of spectral absorption efficiencies Q_abs
 * @param scaSpectralEff - vector of spectral scattering efficiencies Q_sca
 * @param lambdas - vector of wavelengths (m)
 * @param raV - vector of radii (m)
 * @param tempV - vector of temperatures (K)
 * @param minT - minimum temperature to be tabulated (K)
 * @param maxT - maximum temperature to be tabulated (K)
 */
void
mean_coeff( std::vector<double>& absPlanckCoeff,
            std::vector<double>& scaPlanckCoeff,
            std::vector<double>& absRossCoeff,
            std::vector<double>& scaRossCoeff,
            const std::vector<double>& absSpectralEff,
            const std::vector<double>& scaSpectralEff,
            const std::vector<double>& lambdas,
            const std::vector<double>& raV,
            std::vector<double>& tempV,
            const double minT,
            const double maxT )
{
  const int nR = raV.size(); //number of radii to be considered

  const double stepT=600.0;
  const int nTemp= int((maxT-minT)/stepT)+1; //number of temperatures to be considered
  tempV.resize(nTemp);

  const int nTR=nTemp*nR;
  absPlanckCoeff.resize(nTR);
  scaPlanckCoeff.resize(nTR);
  absRossCoeff.resize(nTR);
  scaRossCoeff.resize(nTR);

  const int L = lambdas.size();

  int coeff =0;

  for (int tt=0;tt<nTemp; tt++){
    tempV[tt]=minT+(tt)*stepT;
    for (int r=0;r<nR;r++){

      double integralAbsRoss=0;
      double integralScaRoss=0;
      double integralAbsPlanck=0;
      double integralScaPlanck=0;
      for (int i=0;i<L-1;i++){

        const double c22 = 0.0144;
        const double xsi0=c22/(exp(lambdas[i])*tempV[tt]);
        const double xsi1=c22/(exp(lambdas[i+1])*tempV[tt]);
        const double xsi_step=xsi0-xsi1;

        const double expxsi1 = exp(xsi1);
        const double expxsi0 = exp(xsi0);
        const double xsi1_3 = xsi1*xsi1*xsi1;
        const double xsi0_3 = xsi0*xsi0*xsi0;
        const double xsi1_4 = xsi1_3 * xsi1;
        const double xsi0_4 = xsi0_3 * xsi0;

        integralAbsPlanck += xsi_step*( xsi1_3/(expxsi1-1)*absSpectralEff[r*L+i+1] + xsi0_3*1/(expxsi0-1)*absSpectralEff[r*L+i] );
        integralScaPlanck += xsi_step*( xsi1_3/(expxsi1-1)*scaSpectralEff[r*L+i+1] + xsi0_3*1/(expxsi0-1)*scaSpectralEff[r*L+i] );

        integralAbsRoss += xsi_step*( xsi1_4*expxsi1 / ( (expxsi1-1)*(expxsi1-1) ) / absSpectralEff[r*L+i+1] + xsi0_4*expxsi0/((expxsi0-1)*(expxsi0-1))/absSpectralEff[r*L+i] );
        integralScaRoss += xsi_step*( xsi1_4*expxsi1 / ( (expxsi1-1)*(expxsi1-1) ) / scaSpectralEff[r*L+i+1] + xsi0_4*expxsi0/((expxsi0-1)*(expxsi0-1))/scaSpectralEff[r*L+i] );
      }
      const double expR = exp(raV[r]);
      const double pi4 = M_PI*M_PI*M_PI*M_PI;
      absPlanckCoeff[coeff] = 15*M_PI*expR*expR/      pi4*integralAbsPlanck/2.0;
      scaPlanckCoeff[coeff] = 15*M_PI*expR*expR/      pi4*integralScaPlanck/2.0;
      absRossCoeff  [coeff] = pi4*M_PI*expR*expR/(15.0*integralAbsRoss/2.0);
      scaRossCoeff  [coeff] = pi4*M_PI*expR*expR/(15.0*integralScaRoss/2.0);

#     ifdef RADPROPS_DEBUG_INFO
      cout << tempV[tt] << " " << exp(raV[r]) << " " << absPlanckCoeff[coeff] << " " << scaPlanckCoeff[coeff]<< " " << absRossCoeff[coeff] << " " << scaRossCoeff[coeff] << endl;
#     endif // RADPROPS_DEBUG_INFO
      coeff+=1;
    }
  }
}

//==============================================================================

ParticleRadCoeffs::ParticleRadCoeffs( const std::complex<double> refIndex,
                                      const double minRadius,
                                      const double maxRadius,
                                      const int nRadii,
                                      const unsigned interpOrder )
{
  std::vector<double> absSpectralEff, scaSpectralEff, lambdas, raV;
  spectral_effs( absSpectralEff, scaSpectralEff, lambdas, raV, minRadius, maxRadius,nRadii, refIndex );

  std::vector<double> absSpectralCoeff;
  std::vector<double> scaSpectralCoeff;
  spectral_coeff( absSpectralCoeff, scaSpectralCoeff, absSpectralEff, scaSpectralEff, lambdas, raV );

  const bool clipValues = true;

  std::vector<double> absCoeffRoss, scaCoeffRoss, absCoeffPlanck, scaCoeffPlanck, tempV;

  mean_coeff( absCoeffPlanck, scaCoeffPlanck, absCoeffRoss, scaCoeffRoss, absSpectralEff, scaSpectralEff, lambdas, raV, tempV, 600, 2800 );

  interp2AbsSpectralCoeff_ = new LagrangeInterpolant2D( interpOrder, lambdas, raV, absSpectralCoeff, clipValues );
  interp2ScaSpectralCoeff_ = new LagrangeInterpolant2D( interpOrder, lambdas, raV, scaSpectralCoeff, clipValues );

  interp2AbsPlanckCoeff_ = new LagrangeInterpolant2D( interpOrder, raV, tempV, absCoeffPlanck, clipValues );
  interp2ScaPlanckCoeff_ = new LagrangeInterpolant2D( interpOrder, raV, tempV, scaCoeffPlanck, clipValues );
  interp2AbsRossCoeff_   = new LagrangeInterpolant2D( interpOrder, raV, tempV, absCoeffRoss,   clipValues );
  interp2ScaRossCoeff_   = new LagrangeInterpolant2D( interpOrder, raV, tempV, scaCoeffRoss,   clipValues );
}

ParticleRadCoeffs::~ParticleRadCoeffs()
{
  delete interp2AbsSpectralCoeff_;
  delete interp2ScaSpectralCoeff_;
  delete interp2AbsPlanckCoeff_;
  delete interp2ScaPlanckCoeff_;
  delete interp2AbsRossCoeff_;
  delete interp2ScaRossCoeff_;
}

double
ParticleRadCoeffs::abs_spectral_coeff( const double wavelength, const double r) const{
  double indepVars [] = {log(wavelength), log(r)};
  return interp2AbsSpectralCoeff_->value(indepVars);
}

double
ParticleRadCoeffs::scattering_spectral_coeff( const double wavelength, const double r) const{
  double indepVars [] = {log(wavelength), log(r)};
  return interp2ScaSpectralCoeff_->value(indepVars);
}

double
ParticleRadCoeffs::planck_abs_coeff( const double r, const double T) const{
  double indepVars [] = {log(r),  T };
  return interp2AbsPlanckCoeff_->value(indepVars);
}

double
ParticleRadCoeffs::planck_sca_coeff( const double r, const double T) const{
  double indepVars [] = {log(r),  T };
  return interp2ScaPlanckCoeff_->value(indepVars);
}

double
ParticleRadCoeffs::ross_abs_coeff( const double r, const double T) const{
  double indepVars [] = {log(r),  T };
  return interp2AbsRossCoeff_->value(indepVars);
}

double
ParticleRadCoeffs::ross_sca_coeff( const double r, const double T) const{
  double indepVars [] = {log(r),  T };
  return interp2ScaRossCoeff_->value(indepVars);
}

#ifdef ENABLE_CUDA
void
ParticleRadCoeffs::gpu_abs_spectral_coeff( double* result, const double* const wavelength, const double* const r, const size_t indepsize) const{

  double *logWavelength, *logR;
  cudaMalloc((void**) &logWavelength, sizeof(double) * indepsize);
  cudaMalloc((void**) &logR,          sizeof(double) * indepsize);
  gpu_log<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(logWavelength, wavelength, indepsize);
  gpu_log<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(logR         , r         , indepsize);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK;
  std::vector<const double*> indep(2, NULL);
  indep[0] = logWavelength;
  indep[1] = logR;
  interp2AbsSpectralCoeff_->gpu_value(indep, result, indepsize);
  cudaFree( logWavelength );
  cudaFree( logR          );
}
//-------------------------------------------------------------------
void
ParticleRadCoeffs::gpu_scattering_spectral_coeff( double* result, const double* const wavelength, const double* const r, const size_t indepsize) const{
  
  double *logWavelength, *logR;
  cudaMalloc((void**) &logWavelength, sizeof(double) * indepsize);
  cudaMalloc((void**) &logR,          sizeof(double) * indepsize);
  gpu_log<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(logWavelength, wavelength, indepsize);
  gpu_log<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(logR         , r         , indepsize);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK;
  std::vector<const double*> indep(2, NULL);
  indep[0] = logWavelength;
  indep[1] = logR;
  
  interp2ScaSpectralCoeff_->gpu_value(indep, result, indepsize);
  
  cudaFree( logWavelength );
  cudaFree( logR          );
}
//-------------------------------------------------------------------
void
ParticleRadCoeffs::gpu_planck_abs_coeff( double* result, const double* const r, const double* const T, const size_t indepsize) const{
  
  double  *logR;
  cudaMalloc((void**) &logR, sizeof(double) * indepsize);
  gpu_log<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(logR, r, indepsize);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK;
  std::vector<const double*> indep(2, NULL);
  indep[0] = logR;
  indep[1] = T;
  
  interp2AbsPlanckCoeff_->gpu_value(indep, result, indepsize);
  
  cudaFree( logR );
}
//-------------------------------------------------------------------
void
ParticleRadCoeffs::gpu_planck_sca_coeff( double* result, const double* const r, const double* const T, const size_t indepsize) const{
  
  double  *logR;
  cudaMalloc((void**) &logR, sizeof(double) * indepsize);
  gpu_log<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(logR, r, indepsize);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK;
  std::vector<const double*> indep(2, NULL);
  indep[0] = logR;
  indep[1] = T;
  
  interp2ScaPlanckCoeff_->gpu_value(indep, result, indepsize);
  
  cudaFree( logR );
}
//-------------------------------------------------------------------
void
ParticleRadCoeffs::gpu_ross_abs_coeff( double* result, const double* const r, const double* const T, const size_t indepsize) const{
  
  double  *logR;
  cudaMalloc((void**) &logR, sizeof(double) * indepsize);
  gpu_log<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(logR, r, indepsize);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK;
  std::vector<const double*> indep(2, NULL);
  indep[0] = logR;
  indep[1] = T;
  
  interp2AbsRossCoeff_->gpu_value(indep, result, indepsize);
  
  cudaFree( logR );
}
//-------------------------------------------------------------------
void
ParticleRadCoeffs::gpu_ross_sca_coeff( double* result, const double* const r, const double* const T, const size_t indepsize) const{
  
  double  *logR;
  cudaMalloc((void**) &logR, sizeof(double) * indepsize);
  gpu_log<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(logR, r, indepsize);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK;
  std::vector<const double*> indep(2, NULL);
  indep[0] = logR;
  indep[1] = T;
  
  interp2ScaRossCoeff_->gpu_value(indep, result, indepsize);
  
  cudaFree( logR );
}
#endif
//==============================================================================

ParticleRadCoeffs3D::ParticleRadCoeffs3D( const std::complex<double> refIndexLo,
                                          const std::complex<double> refIndexHi,
                                          const int nRes,
                                          const double minRadius,
                                          const double maxRadius,
                                          const int nRadii, //number of radii to be considered
                                          const unsigned interpOrder )
{
//------------------- Dependent variables------------------ /// 
  std::vector< std::vector<double> >                        ///
                       absCoeffRoss(nRes),                  /// absorption coefficient Rosseland
                       scaCoeffRoss(nRes),                  /// scattering coefficient Rosseland
                       absCoeffPlanck(nRes),                /// absorption coefficient Planck 
                       scaCoeffPlanck(nRes),                /// scattering coefficient Planck
                       absSpectralCoeff(nRes),              /// Absorption spectral Coefficient
                       scaSpectralCoeff(nRes);              /// Scattering spectral Coefficient
  std::vector<double>  absCoeffRoss3D,                      ///  
                       scaCoeffRoss3D,                      /// ------------------------------ ///
                       absCoeffPlanck3D,                    /// A repeat of the above variables,
                       scaCoeffPlanck3D,                    /// using a different type so that  
                       absSpectralCoeff3D,                  /// they can interface with tabprops
                       scaSpectralCoeff3D;                  /// ------------------------------ /// 
//------------------- Independent variables---------------- ///
  std::vector<double> refIndexReal(nRes),                   /// Real component of the complex index of refraction
                      raV,                                  /// natural log of the particle radius 
                      tempV,                                /// Temperature of the particles
                      lambdas;                              /// wavelength, lambda
// ------------------ Intermediate variables -------------- ///
  std::vector<double>  absSpectralEff,                      /// absorption spectral efficiency
                       scaSpectralEff;                      /// scattering spectral efficiency
//------------------- Table parameters -------------------- ///

  const bool clipValues = true;
                                                            
  if( nRes < 2 ){
    std::ostringstream msg;
    msg << __FILE__ << " : " << __LINE__
        << "\nERROR constructing ParticleRadCoeffs3D:\n"
        << "\tthe third dimension must have 2 or more points!\n"
        << "\tUse 2D version of RadProps for 1 data point in space\n"
        << "\tof complex index of refraction.\n";
    throw std::invalid_argument( msg.str() );
  }

  std::complex<double> deltaRefIndex ((refIndexHi.real()-refIndexLo.real())/(nRes-1),(refIndexHi.imag()-refIndexLo.imag())/(nRes-1));


  for( size_t i=0; i< nRes; i++ ){


    const std::complex<double> refIndex(refIndexLo.real() + (double) i*deltaRefIndex.real(), refIndexLo.imag() + (double) i*deltaRefIndex.imag()  );

    refIndexReal[i]= refIndex.real();

#   ifdef RADPROPS_DEBUG_INFO
    cout <<"RadProps: Building table for complex index of refraction -> "<< refIndex.real() << " and  " << refIndex.imag() << "i  \n";
#   endif

    spectral_effs( absSpectralEff, scaSpectralEff, lambdas, raV, minRadius, maxRadius,nRadii, refIndex );

    spectral_coeff( absSpectralCoeff[i], scaSpectralCoeff[i], absSpectralEff, scaSpectralEff, lambdas, raV );

    mean_coeff( absCoeffPlanck[i], scaCoeffPlanck[i], absCoeffRoss[i], scaCoeffRoss[i], absSpectralEff, scaSpectralEff, lambdas, raV, tempV, 600, 2800 );
  }

  size_t L=lambdas.size(); // length of 3D table
  size_t W=raV.size();     // width of 3D table
  size_t D=nRes;           // depth of 3D table
  absSpectralCoeff3D.resize(L*W*D);
  scaSpectralCoeff3D.resize(L*W*D);
  for( size_t j=0; j< nRes; j++ ){
    for( size_t i=0; i< L*W; i++ ){
      absSpectralCoeff3D[i + j*L*W]=absSpectralCoeff[j][i];
      scaSpectralCoeff3D[i + j*L*W]=scaSpectralCoeff[j][i];
    }
  }

  L=raV.size();     // length of 3D table
  W=tempV.size();   // width of 3D table
  D=nRes;           // depth of 3D table
  absCoeffPlanck3D.resize(L*W*D);
  scaCoeffPlanck3D.resize(L*W*D);
  absCoeffRoss3D.resize(L*W*D);
  scaCoeffRoss3D.resize(L*W*D);  
  for( size_t j=0; j< nRes; j++ ){
    for( size_t i=0; i< L*W; i++ ){
      absCoeffPlanck3D[i + j*L*W]=absCoeffPlanck[j][i];
      scaCoeffPlanck3D[i + j*L*W]=scaCoeffPlanck[j][i];
      absCoeffRoss3D  [i + j*L*W]=absCoeffRoss  [j][i];
      scaCoeffRoss3D  [i + j*L*W]=scaCoeffRoss  [j][i];
    }
  }

  interp2AbsSpectralCoeff_ = new LagrangeInterpolant3D( interpOrder, lambdas, raV, refIndexReal, absSpectralCoeff3D, clipValues );
  interp2ScaSpectralCoeff_ = new LagrangeInterpolant3D( interpOrder, lambdas, raV, refIndexReal, scaSpectralCoeff3D, clipValues );

  interp2AbsPlanckCoeff_ = new LagrangeInterpolant3D( interpOrder, raV, tempV, refIndexReal, absCoeffPlanck3D, clipValues );
  interp2ScaPlanckCoeff_ = new LagrangeInterpolant3D( interpOrder, raV, tempV, refIndexReal, scaCoeffPlanck3D, clipValues );
  interp2AbsRossCoeff_   = new LagrangeInterpolant3D( interpOrder, raV, tempV, refIndexReal, absCoeffRoss3D,   clipValues );
  interp2ScaRossCoeff_   = new LagrangeInterpolant3D( interpOrder, raV, tempV, refIndexReal, scaCoeffRoss3D,   clipValues );
}

ParticleRadCoeffs3D::~ParticleRadCoeffs3D()
{
  delete interp2AbsSpectralCoeff_;
  delete interp2ScaSpectralCoeff_;
  delete interp2AbsPlanckCoeff_;
  delete interp2ScaPlanckCoeff_;
  delete interp2AbsRossCoeff_;
  delete interp2ScaRossCoeff_;
}

double
ParticleRadCoeffs3D::abs_spectral_coeff( const double wavelength, const double r, const double IRreal) const{
  double indepVars [] = {log(wavelength), log(r), IRreal};
  return interp2AbsSpectralCoeff_->value(indepVars);
}

double
ParticleRadCoeffs3D::scattering_spectral_coeff( const double wavelength, const double r, const double IRreal) const{
  double indepVars [] = {log(wavelength), log(r), IRreal};
  return interp2ScaSpectralCoeff_->value(indepVars);
}

double
ParticleRadCoeffs3D::planck_abs_coeff( const double r, const double T, const double IRreal) const{
  double indepVars [] = {log(r),  T, IRreal };
  return interp2AbsPlanckCoeff_->value(indepVars);
}

double
ParticleRadCoeffs3D::planck_sca_coeff( const double r, const double T, const double IRreal) const{
  double indepVars [] = {log(r),  T, IRreal };
  return interp2ScaPlanckCoeff_->value(indepVars);
}

double
ParticleRadCoeffs3D::ross_abs_coeff( const double r, const double T, const double IRreal) const{
  double indepVars [] = {log(r),  T, IRreal};
  return interp2AbsRossCoeff_->value(indepVars);
}

double
ParticleRadCoeffs3D::ross_sca_coeff( const double r, const double T, const double IRreal) const{
  double indepVars [] = {log(r),  T, IRreal };
  return interp2ScaRossCoeff_->value(indepVars);
}
//-------------------------------------------------------------------
#ifdef ENABLE_CUDA
void
ParticleRadCoeffs3D::gpu_abs_spectral_coeff( double* result, const double* const wavelength, const double* const r, const double* IRreal, const size_t indepsize) const{
  
  double *logWavelength, *logR;
  cudaMalloc((void**) &logWavelength, sizeof(double) * indepsize);
  cudaMalloc((void**) &logR,          sizeof(double) * indepsize);
  gpu_log<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(logWavelength, wavelength, indepsize);
  gpu_log<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(logR         , r         , indepsize);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK;
  std::vector<const double*> indep(3, NULL);
  indep[0] = logWavelength;
  indep[1] = logR;
  indep[2] = IRreal;
  
  interp2AbsSpectralCoeff_->gpu_value(indep, result, indepsize);
  
  cudaFree( logWavelength );
  cudaFree( logR          );
}
//-------------------------------------------------------------------
void
ParticleRadCoeffs3D::gpu_scattering_spectral_coeff( double* result, const double* const wavelength, const double* const r, const double* IRreal, const size_t indepsize) const{
  
  double *logWavelength, *logR;
  cudaMalloc((void**) &logWavelength, sizeof(double) * indepsize);
  cudaMalloc((void**) &logR,          sizeof(double) * indepsize);
  gpu_log<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(logWavelength, wavelength, indepsize);
  gpu_log<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(logR         , r         , indepsize);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK;
  std::vector<const double*> indep(3, NULL);
  indep[0] = logWavelength;
  indep[1] = logR;
  indep[2] = IRreal;
  
  interp2ScaSpectralCoeff_->gpu_value(indep, result, indepsize);
  
  cudaFree( logWavelength );
  cudaFree( logR          );
}
//-------------------------------------------------------------------
void
ParticleRadCoeffs3D::gpu_planck_abs_coeff( double* result, const double* const r, const double* const T, const double* IRreal, const size_t indepsize) const{
  
  double  *logR;
  cudaMalloc((void**) &logR, sizeof(double) * indepsize);
  gpu_log<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(logR, r, indepsize);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK;
  std::vector<const double*> indep(3, NULL);
  indep[0] = logR;
  indep[1] = T;
  indep[2] = IRreal;
  
  interp2AbsPlanckCoeff_->gpu_value(indep, result, indepsize);
  
  cudaFree( logR );
}
//-------------------------------------------------------------------
void
ParticleRadCoeffs3D::gpu_planck_sca_coeff( double* result, const double* const r, const double* const T, const double* IRreal, const size_t indepsize) const{
  
  double  *logR;
  cudaMalloc((void**) &logR, sizeof(double) * indepsize);
  gpu_log<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(logR, r, indepsize);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK;
  std::vector<const double*> indep(3, NULL);
  indep[0] = logR;
  indep[1] = T;
  indep[2] = IRreal;
  
  interp2ScaPlanckCoeff_->gpu_value(indep, result, indepsize);
  
  cudaFree( logR );
}
//-------------------------------------------------------------------
void
ParticleRadCoeffs3D::gpu_ross_abs_coeff( double* result, const double* const r, const double* const T, const double* IRreal, const size_t indepsize) const{
  
  double  *logR;
  cudaMalloc((void**) &logR, sizeof(double) * indepsize);
  gpu_log<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(logR, r, indepsize);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK;
  std::vector<const double*> indep(3, NULL);
  indep[0] = logR;
  indep[1] = T;
  indep[2] = IRreal;
  
  interp2AbsRossCoeff_->gpu_value(indep, result, indepsize);
  
  cudaFree( logR );
}
//-------------------------------------------------------------------
void
ParticleRadCoeffs3D::gpu_ross_sca_coeff( double* result, const double* const r, const double* const T, const double* IRreal, const size_t indepsize) const{
  
  double  *logR;
  cudaMalloc((void**) &logR, sizeof(double) * indepsize);
  gpu_log<<<NBLOCK(indepsize), NTHREAD(indepsize)>>>(logR, r, indepsize);
  cudaThreadSynchronize();
  GPU_ERROR_CHECK;
  std::vector<const double*> indep(3, NULL);
  indep[0] = logR;
  indep[1] = T;
  indep[2] = IRreal;
  
  interp2ScaRossCoeff_->gpu_value(indep, result, indepsize);
  
  cudaFree( logR );
}
#endif // ENABLE_CUDA
} // namespace RadProps
//==============================================================================

