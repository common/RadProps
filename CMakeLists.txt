cmake_minimum_required( VERSION 3.10 )

project( RadProps )

set( CMAKE_CXX_STANDARD 11 )

include( CMakePackageConfigHelpers )

# warn about building in source
if( CMAKE_SOURCE_DIR STREQUAL CMAKE_BINARY_DIR )
  message( WARNING "\nIt appears that you are building directly in the source directory.\n"
    "This is strongly discouraged.\n"
    "Suggestion:\n\tmkdir build; cd build\n"
    "Then run cmake from the 'build' directory" ) 
endif( CMAKE_SOURCE_DIR STREQUAL CMAKE_BINARY_DIR )

set( MAJOR_VERSION "1" )
set( MINOR_VERSION "0" )
set( PATCH_VERSION "0" )

option( BUILD_SHARED_LIBS "Use shared/dynamic libraries" OFF )
option( RadProps_ENABLE_TESTING "enable regression tests" ON )
option( RadProps_ENABLE_PREPROCESSOR "enable preprocessing and table querying utilities" ON )
option( ENABLE_CUDA   "Build RadProps with CUDA support" OFF )
option( BUILD_TABPROPS "Build TabProps internally" OFF )

set( TabProps_Install_DIR "" CACHE PATH "Path to installation of TabProps" )

include( RadBuildTools.cmake )

# Set the location for the installation.  Comment this out to install
# to /usr/local.  Or modify it to install to a different location.
if(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
  set( CMAKE_INSTALL_PREFIX
    ${CMAKE_CURRENT_BINARY_DIR}
    CACHE PATH "Installation directory" FORCE
    )
endif()

# default to release builds
if( NOT CMAKE_BUILD_TYPE )
  set( CMAKE_BUILD_TYPE "Release" CACHE STRING
    "Choose the type of build, options are: Debug Release RelWithDebInfo MinSizeRel."
    FORCE )
endif( NOT CMAKE_BUILD_TYPE )

find_package( Git )
if( GIT_FOUND )
  execute_process(
      COMMAND ${GIT_EXECUTABLE} log -1 "--pretty=format:\"%H\""
      WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
      OUTPUT_VARIABLE RADPROPS_REPO_HASH
      OUTPUT_STRIP_TRAILING_WHITESPACE
    )
  execute_process(
      COMMAND ${GIT_EXECUTABLE} log -1 "--pretty=format:\"%cd\""
      WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
      OUTPUT_VARIABLE RADPROPS_REPO_DATE
      OUTPUT_STRIP_TRAILING_WHITESPACE
    )
else()
  set( RADPROPS_REPO_DATE "\"No information available\"" )
  set( RADPROPS_REPO_HASH "\"No information available\"" )
endif()

if( RadProps_ENABLE_TESTING )
  set( RadProps_ENABLE_PREPROCESSOR ON )
endif() 

# grab TabProps and build the linear interpolants
if( BUILD_TABPROPS )
  message( STATUS "Building TabProps internally..." )
  if( NOT GIT_FOUND )
    message( SEND_ERROR "git required to build TabProps" )
  endif()
  set( TP_DIR ${CMAKE_CURRENT_BINARY_DIR}/TabProps )
  file( MAKE_DIRECTORY ${TP_DIR}/build )
  message( "${GIT_EXECUTABLE} clone --depth 1 https://gitlab.multiscale.utah.edu/common/TabProps.git ${TP_DIR}/src" )
  execute_process( COMMAND ${GIT_EXECUTABLE} clone --depth 1 https://gitlab.multiscale.utah.edu/common/TabProps.git ${TP_DIR}/src
          RESULT_VARIABLE result )
  # configure
  execute_process( COMMAND ${CMAKE_COMMAND} -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} -DTabProps_PREPROCESSOR=OFF;-DTabProps_UTILS=OFF;-DTabProps_ENABLE_TESTING=OFF -DENABLE_CUDA=${ENABLE_CUDA} -DCMAKE_INSTALL_PREFIX=${TP_DIR} ${TP_DIR}/src
          WORKING_DIRECTORY ${TP_DIR}/build
          RESULT_VARIABLE result  )
  # build / install
  execute_process( COMMAND make -j4 install
          WORKING_DIRECTORY ${TP_DIR}/build )
  set( TabProps_Install_DIR ${TP_DIR}/share )
  message( STATUS "TabProps dir: ${TP_DIR}\n\t${TabProps_Install_DIR}" )
  message("\n\n DONE BUILDING TABPROPS \n\n")
  message( STATUS "TabProps installation is in ${TP_DIR}" )
endif( BUILD_TABPROPS )

find_package( TabProps PATHS ${TabProps_Install_DIR} REQUIRED COMPONENTS interp CONFIG NO_DEFAULT_PATH PATH_SUFFIXES share )
if( NOT TabProps_FOUND )
  message( FATAL_ERROR "TabProps::interp is required for RadProps, but was not found." )
else()
  message( STATUS "TabProps found at ${TabProps_INCLUDE_DIR}" )
endif()


#-- boost libraries (only needed for the executables)
if( RadProps_ENABLE_TESTING OR RadProps_ENABLE_PREPROCESSOR )
  
  set( boost_comps "program_options;serialization;regex;filesystem;system" )

  if( DEFINED BOOST_INCLUDEDIR AND DEFINED BOOST_LIBRARYDIR )
    message( STATUS "Setting boost include location as: " ${BOOST_INCLUDEDIR} )
    message( STATUS "Setting boost library location as: " ${BOOST_LIBRARYDIR} )
    set( Boost_NO_SYSTEM_PATHS ON )  # only look in the user-specified location - nowhere else!
  endif()
  
  if( DEFINED BOOST_ROOT )
    set( BOOST_DIR ${Boost_DIR} )  # find_boost breaks the supplied value of Boost_DIR
    # give location hints.
    message( STATUS "Setting boost installation location as: " ${Boost_DIR} )
    set( Boost_NO_SYSTEM_PATHS ON )  # only look in the user-specified location - nowhere else!
    set( TP_CMAKE_ARGS "-DBoost_DIR=${BOOST_DIR};" ${TP_CMAKE_ARGS} )
  endif( DEFINED BOOST_ROOT )
  
  if( DEFINED BOOST_INCLUDEDIR AND DEFINED BOOST_LIBRARYDIR )
    message( STATUS "Setting boost include location as: " ${BOOST_INCLUDEDIR} )
    message( STATUS "Setting boost library location as: " ${BOOST_LIBRARYDIR} )
    set( Boost_NO_SYSTEM_PATHS ON )  # only look in the user-specified location - nowhere else!
  endif()
  
  set( BOOST_MIN_VERSION "1.49.0" )
  
  find_package( Boost ${BOOST_MIN_VERSION} REQUIRED COMPONENTS ${boost_comps} )
  
  message( STATUS "RadProps: Boost header installation found at: " ${Boost_INCLUDE_DIR} )
  message( STATUS "RadProps: Boost library installation found at: " ${Boost_LIBRARY_DIRS} )
  message( STATUS "RadProps: Boost version: " ${Boost_MAJOR_VERSION}.${Boost_MINOR_VERSION}.${Boost_SUBMINOR_VERSION} )
  message( STATUS "RadProps: Boost libs: ${Boost_LIBRARIES}")

endif()


#-- CUDA
if( ENABLE_CUDA )
  add_definitions( -DENABLE_CUDA=${ENABLE_CUDA}) 
  message( STATUS "RadProps will be compiled with CUDA Support" )
  find_package( CUDA REQUIRED )
  set( CUDA_ARCHITECTURE_MINIMUM "3.0" CACHE STRING "Minimum required CUDA compute capability" )
  if( NOT DISABLE_INTROSPECTION )
    try_run( RUN_RESULT_VAR COMPILE_RESULT_VAR
        ${CMAKE_BINARY_DIR} 
        ${CMAKE_SOURCE_DIR}/cudaComputeCapability.cpp
        CMAKE_FLAGS 
        -DINCLUDE_DIRECTORIES:STRING=${CUDA_TOOLKIT_INCLUDE}
        -DLINK_LIBRARIES:STRING=${CUDA_CUDART_LIBRARY}
        COMPILE_OUTPUT_VARIABLE COMPILE_OUTPUT_VAR
        RUN_OUTPUT_VARIABLE RUN_OUTPUT_VAR
        ARGS ${CUDA_ARCHITECTURE_MINIMUM} )
        message( STATUS ${COMPILE_OUTPUT_VAR} )
    if( NOT ${RUN_RESULT_VAR} EQUAL 0 OR NOT ${COMPILE_RESULT_VAR} )
      message( FATAL_ERROR "Problem determining an appropriate CUDA architecture\n"
                \t${RUN_OUTPUT_VAR}\n
                \t${RUN_RESULT_VAR}
              )
    endif()
    message( STATUS "CUDA COMPUTE CAPABILITY: ${RUN_OUTPUT_VAR}" )
    set( CUDA_ARCHITECTURE ${RUN_OUTPUT_VAR} CACHE STRING "CUDA compute capability" )    
  else()
    set( CUDA_ARCHITECTURE ${CUDA_ARCHITECTURE_MINIMUM} CACHE STRING "CUDA compute capability" )
  endif()
  string( REGEX REPLACE "([1-9])\\.([0-9])" "\\1\\2" CUDA_ARCH_STR ${CUDA_ARCHITECTURE} )
  set( CUDA_NVCC_FLAGS "-arch=sm_${CUDA_ARCH_STR}" CACHE STRING "Compiler flags passed to NVCC" FORCE )
  set( CUDA_NVCC_FLAGS_RELEASE "--optimize=03" CACHE STRING "Release build compiler flags" FORCE )

else( ENABLE_CUDA )
  # avoids confusion if someone tries to build with cuda support enabled and then
  # reverts without clearing the build directory
  SET( CUDA_CUDA_LIBRARY OFF )
endif( ENABLE_CUDA )
#--------------------------------------

configure_file( RadPropsConfig.h.in ${CMAKE_BINARY_DIR}/radprops/RadPropsConfig.h )
list( APPEND RAD_H ${CMAKE_BINARY_DIR}/radprops/RadPropsConfig.h )

add_subdirectory( radprops )

if( RadProps_ENABLE_TESTING )
  enable_testing()
  add_subdirectory( test )
endif()

