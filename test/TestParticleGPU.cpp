/*
 * Copyright (c) 2014-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


/*
   Orion Sky Lawlor, olawlor@acm.org, 9/23/2000

   Computes so-called "Mie scattering" for a homogenous
   sphere of arbitrary size illuminated by coherent
   harmonic radiation.
 */

#include <radprops/Particles.h>

#include <test/TestHelper.h>

#include <iostream>
#include <iomanip>
#include <vector>
#include <ctime>
#include <string>

#include "cuda.h"

using namespace std;
using namespace RadProps;

bool cpu_gpu_compare(const double* gpuValues, const double* cpuValues, const size_t n, const double t_cpu, const double t_gpu)
{
  double *hostValues;
  hostValues = (double*)malloc(sizeof(double) * n);
  cudaError_t err= cudaMemcpy( hostValues, gpuValues, sizeof(double) * n, cudaMemcpyDeviceToHost );
  cudaThreadSynchronize();
  for (int i=0; i<n; ++i) {
    if (std::abs((hostValues[i] - cpuValues[i]) / cpuValues[i])  > 1e-8) {
      std::cout << cpuValues[i]  <<  " - " <<hostValues[i] << "\n";
      return false;
    }
  }
  std::cout <<" npts = " << n << "\t" << "time CPU = " << t_cpu << ",  GPU = " << t_gpu << "  => CPU/GPU = " << t_cpu / t_gpu << std::endl;
  return true;
}

bool time_it2D(const ParticleRadCoeffs& pcoeff, const ParticleRadCoeffs3D& pcoeff3D, const size_t n)
{
  
  TestHelper status(true);
  std::vector<double> wavelength, radius, temperature, iRreal;
  for (int i=0; i<n ; i++) {
    wavelength. push_back(double(rand())/double(RAND_MAX) * 1e-6);
    radius.     push_back(double(rand())/double(RAND_MAX) * 1e-6);
    temperature.push_back(double(rand())/double(RAND_MAX) * 1000 + 300);
    iRreal.     push_back(double(rand())/double(RAND_MAX));
  }

  // GPU variables
  double  *gpuWavelength, *gpuRadius, *gpuTemperature, *gpuiRreal;
  
  cudaMalloc((void**) &gpuWavelength,   sizeof(double) * n);
  cudaMalloc((void**) &gpuRadius,       sizeof(double) * n);
  cudaMalloc((void**) &gpuTemperature,  sizeof(double) * n);
  cudaMalloc((void**) &gpuiRreal,       sizeof(double) * n);
  
  cudaMemcpy(gpuWavelength  , &wavelength[0], sizeof(double) * n, cudaMemcpyHostToDevice);
  cudaMemcpy(gpuRadius      , &radius[0],     sizeof(double) * n, cudaMemcpyHostToDevice);
  cudaMemcpy(gpuTemperature , &temperature[0],sizeof(double) * n, cudaMemcpyHostToDevice);
  cudaMemcpy(gpuiRreal      , &gpuiRreal[0],  sizeof(double) * n, cudaMemcpyHostToDevice);
  cudaThreadSynchronize();
  
  double *gpuValues;
  cudaMalloc((void**) &gpuValues, sizeof(double) * n);

  std::clock_t start;
  double t_cpu, t_gpu;
  string modelname;
  
  
  modelname = "abs_spectral_coeff";
  std::vector<double> cpuValues;
  cpuValues.clear();
  // 2D
  // Run on CPU
  start = std::clock();
  for( int unsigned i=0; i<n; ++i ) cpuValues.push_back(pcoeff.abs_spectral_coeff(wavelength[i], radius[i]));
  t_cpu = std::clock() - start;
  // Run on GPU
  start = std::clock(); pcoeff.gpu_abs_spectral_coeff(gpuValues, gpuWavelength, gpuRadius, n); t_gpu = std::clock() - start;
  status( cpu_gpu_compare(gpuValues, &cpuValues[0], n, t_cpu, t_gpu), modelname);
  
  // 3D
  cpuValues.clear();
  start = std::clock();
  for( int unsigned i=0; i<n; ++i ) cpuValues.push_back(pcoeff3D.abs_spectral_coeff(wavelength[i], radius[i], iRreal[i]));
  t_cpu = std::clock() - start;
  start = std::clock(); pcoeff3D.gpu_abs_spectral_coeff(gpuValues, gpuWavelength, gpuRadius, gpuiRreal, n); t_gpu = std::clock() - start;
  status( cpu_gpu_compare(gpuValues, &cpuValues[0], n, t_cpu, t_gpu), modelname + " 3D");
  
  
  modelname = "scattering_spectral_coeff";
  cpuValues.clear();
  // 2D
  // Run on CPU
  start = std::clock();
  for( int unsigned i=0; i<n; ++i ) cpuValues[i] = pcoeff.scattering_spectral_coeff(wavelength[i], radius[i]);
  t_cpu = std::clock() - start;
  // Run on GPU
  start = std::clock(); pcoeff.gpu_scattering_spectral_coeff(gpuValues, gpuWavelength, gpuRadius, n); t_gpu = std::clock() - start;
  status( cpu_gpu_compare(gpuValues, &cpuValues[0], n, t_cpu, t_gpu), modelname);
  // 3D
  // Run on CPU
  cpuValues.clear();
  start = std::clock();
  for( int unsigned i=0; i<n; ++i ) cpuValues[i] = pcoeff3D.scattering_spectral_coeff(wavelength[i], radius[i], iRreal[i]);
  t_cpu = std::clock() - start;
  // Run on GPU
  start = std::clock(); pcoeff3D.gpu_scattering_spectral_coeff(gpuValues, gpuWavelength, gpuRadius, gpuiRreal, n); t_gpu = std::clock() - start;
  status( cpu_gpu_compare(gpuValues, &cpuValues[0], n, t_cpu, t_gpu), modelname + " 3D");

  
  //  -----------    planck_abs_coeff
  // 2D
  // Run on CPU
  modelname = "planck_abs_coeff";
  cpuValues.clear();
  start = std::clock();
  for( int unsigned i=0; i<n; ++i ) cpuValues.push_back(pcoeff.planck_abs_coeff(radius[i], temperature[i]));
  t_cpu = std::clock() - start;
  // Run on GPU
  start = std::clock(); pcoeff.gpu_planck_abs_coeff(gpuValues, gpuRadius, gpuTemperature, n); t_gpu = std::clock() - start;
  status( cpu_gpu_compare(gpuValues, &cpuValues[0], n, t_cpu, t_gpu), modelname);
  // 3D
  // Run on CPU
  cpuValues.clear();
  start = std::clock();
  for( int unsigned i=0; i<n; ++i ) cpuValues.push_back(pcoeff3D.planck_abs_coeff(radius[i], temperature[i], iRreal[i]));
  t_cpu = std::clock() - start;
  // Run on GPU
  start = std::clock(); pcoeff3D.gpu_planck_abs_coeff(gpuValues, gpuRadius, gpuTemperature,gpuiRreal, n); t_gpu = std::clock() - start;
  status( cpu_gpu_compare(gpuValues, &cpuValues[0], n, t_cpu, t_gpu), modelname + " 3D");


  //  -----------    planck_sca_coeff
  // Run on CPU
  modelname = "planck_sca_coeff";
  cpuValues.clear();
  start = std::clock();
  for( int unsigned i=0; i<n; ++i ) cpuValues.push_back(pcoeff.planck_sca_coeff(radius[i], temperature[i]));
  t_cpu = std::clock() - start;
  // Run on GPU
  start = std::clock(); pcoeff.gpu_planck_sca_coeff(gpuValues, gpuRadius, gpuTemperature, n); t_gpu = std::clock() - start;
  status( cpu_gpu_compare(gpuValues, &cpuValues[0], n, t_cpu, t_gpu), modelname);
  
  // 3D
  cpuValues.clear();
  start = std::clock();
  for( int unsigned i=0; i<n; ++i ) cpuValues.push_back(pcoeff3D.planck_sca_coeff(radius[i], temperature[i], iRreal[i]));
  t_cpu = std::clock() - start;
  // Run on GPU
  start = std::clock(); pcoeff3D.gpu_planck_sca_coeff(gpuValues, gpuRadius, gpuTemperature, gpuiRreal, n); t_gpu = std::clock() - start;
  status( cpu_gpu_compare(gpuValues, &cpuValues[0], n, t_cpu, t_gpu), modelname + " 3D");

  
  //  -----------    ross_abs_coeff
  // 2D
  // Run on CPU
  modelname = "ross_abs_coeff";
  cpuValues.clear();
  start = std::clock();
  for( int unsigned i=0; i<n; ++i ) cpuValues.push_back(pcoeff.ross_abs_coeff(radius[i], temperature[i]));
  t_cpu = std::clock() - start;
  // Run on GPU
  start = std::clock(); pcoeff.gpu_ross_abs_coeff(gpuValues, gpuRadius, gpuTemperature, n); t_gpu = std::clock() - start;
  // compare CPU and GPU results
  status( cpu_gpu_compare(gpuValues, &cpuValues[0], n, t_cpu, t_gpu), modelname);

  // 3D
  cpuValues.clear();
  start = std::clock();
  for( int unsigned i=0; i<n; ++i ) cpuValues.push_back(pcoeff3D.ross_abs_coeff(radius[i], temperature[i], iRreal[i]));
  t_cpu = std::clock() - start;
  // Run on GPU
  start = std::clock(); pcoeff3D.gpu_ross_abs_coeff(gpuValues, gpuRadius, gpuTemperature, gpuiRreal, n); t_gpu = std::clock() - start;
  status( cpu_gpu_compare(gpuValues, &cpuValues[0], n, t_cpu, t_gpu), modelname + " 3D");

  //  -----------    ross_sca_coeff
  // Run on CPU
  modelname = "ross_sca_coeff";
  cpuValues.clear();
  start = std::clock();
  for( int unsigned i=0; i<n; ++i ) cpuValues.push_back(pcoeff.ross_sca_coeff(radius[i], temperature[i]));
  t_cpu = std::clock() - start;
  // Run on GPU
  start = std::clock(); pcoeff.gpu_ross_sca_coeff(gpuValues, gpuRadius, gpuTemperature, n); t_gpu = std::clock() - start;
  status( cpu_gpu_compare(gpuValues, &cpuValues[0], n, t_cpu, t_gpu), modelname);
  
  // 3D
  cpuValues.clear();
  start = std::clock();
  for( int unsigned i=0; i<n; ++i ) cpuValues.push_back(pcoeff3D.ross_sca_coeff(radius[i], temperature[i], iRreal[i]));
  t_cpu = std::clock() - start;
  // Run on GPU
  start = std::clock(); pcoeff3D.gpu_ross_sca_coeff(gpuValues, gpuRadius, gpuTemperature, gpuiRreal, n); t_gpu = std::clock() - start;
  status( cpu_gpu_compare(gpuValues, &cpuValues[0], n, t_cpu, t_gpu), modelname + " 3D");

  
  return status.ok();

  cudaFree( gpuWavelength   );
  cudaFree( gpuRadius       );
  cudaFree( gpuTemperature  );
  cudaFree( gpuiRreal       );
  cudaFree( gpuValues       );
}

//==============================================================================
int main( int argc, char* argv[] )
{
  
  TestHelper status(true);
  try{
    complex<double> rCoal, rCoallo, rCoalhi;
    rCoal   =complex<double>(2.0,-0.6);
    rCoallo =complex<double>(0,0);
    rCoalhi =complex<double>(1,1);
    ParticleRadCoeffs   P2(rCoal,1e-7,1e-4,10,1);
    std::cout << " 2D table is made! \n";
    ParticleRadCoeffs3D P3(rCoallo, rCoalhi,5,1e-7,1e-4,5,1);
    std::cout << " 3D table is made! \n";
    status (time_it2D(P2, P3, 2), " Checking GPU versus CPU results ");
            
    if( status.ok() ){
      cout << "PASS" << endl;
      return 0;
    }
    
  }
  catch( std::exception& err ){
    cout << err.what() << endl;
  }
  cout << "FAIL" << endl;
  return -1;

}
