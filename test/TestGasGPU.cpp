/*
 * Copyright (c) 2014-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


/*
   Orion Sky Lawlor, olawlor@acm.org, 9/23/2000

   Computes so-called "Mie scattering" for a homogenous
   sphere of arbitrary size illuminated by coherent
   harmonic radiation.
 */

#include <radprops/AbsCoeffGas.h>

#include <test/TestHelper.h>

#include <iostream>
#include <iomanip>
#include <vector>
#include <ctime>

#include "cuda.h"

using namespace std;
using namespace RadProps;
bool time_it(GreyGas gas, const size_t n)
{
  bool status = true;
  std::vector<double> temperature(n, 0), co2molefrac(n, 0), h2omolefrac(n, 0) ;
  for (int i=0; i<n ; i++) {
    temperature[i] = double(rand())/double(RAND_MAX) * 500 + 1000;
    co2molefrac[i] = double(rand())/double(RAND_MAX);
    h2omolefrac[i] = double(rand())/double(RAND_MAX);
    // normalize co2 and h2o
    const double sum = co2molefrac[i] + h2omolefrac[i];
    co2molefrac[i] /= sum;
    h2omolefrac[i] /= sum;
  }

  // GPU variables
  double  *gpuTemperature, *gpuCO2, *gpuH2O;
  
  cudaMalloc((void**) &gpuTemperature,  sizeof(double) * n);
  cudaMalloc((void**) &gpuCO2,          sizeof(double) * n);
  cudaMalloc((void**) &gpuH2O,          sizeof(double) * n);

  
  cudaMemcpy(gpuTemperature,  &temperature[0], sizeof(double) * n, cudaMemcpyHostToDevice);
  cudaMemcpy(gpuCO2,          &co2molefrac[0], sizeof(double) * n, cudaMemcpyHostToDevice);
  cudaMemcpy(gpuH2O,          &h2omolefrac[0], sizeof(double) * n, cudaMemcpyHostToDevice);
  cudaThreadSynchronize();
  
  // Run on CPU
  std::vector<double> cpuPlank(n, 0), cpuRoss(n, 0), cpuEffabs(n, 0);
  std::clock_t start;
  start = std::clock();
  for( int unsigned i=0; i<n; ++i ) {
    std::vector<double> mixmolefrac(2, 0);
    mixmolefrac[0] = co2molefrac[i];
    mixmolefrac[1] = h2omolefrac[i];
    gas.mixture_coeffs(cpuPlank[i], mixmolefrac, temperature[i], PLANK_COEFF  );
    gas.mixture_coeffs(cpuRoss[i],  mixmolefrac, temperature[i], ROSS_COEFF   );
    gas.mixture_coeffs(cpuEffabs[i],mixmolefrac, temperature[i], EFF_ABS_COEFF);
  }
  const double t_cpu = std::clock() - start;
  // Run on GPU
  
  double *gpuPlank, *gpuRoss, *gpuEffabs;
  cudaMalloc((void**) &gpuPlank,  sizeof(double) * n);
  cudaMalloc((void**) &gpuRoss,   sizeof(double) * n);
  cudaMalloc((void**) &gpuEffabs, sizeof(double) * n);
  
  std::vector<const double*> molefracs(2);
  molefracs[0] = gpuCO2;
  molefracs[1] = gpuH2O;
  
  start = std::clock();
  gas.gpu_mixture_coeffs(gpuPlank, molefracs, gpuTemperature, n, PLANK_COEFF  );
  gas.gpu_mixture_coeffs(gpuRoss,  molefracs, gpuTemperature, n, ROSS_COEFF   );
  gas.gpu_mixture_coeffs(gpuEffabs,molefracs, gpuTemperature, n, EFF_ABS_COEFF);

//  cudaThreadSynchronize();
  const double t_gpu = std::clock() - start;
  // compare CPU and GPU results
  
  double *hostPlank, *hostRoss, *hostEffabs;
  hostPlank   = (double*)malloc(sizeof(double) * n);
  hostRoss    = (double*)malloc(sizeof(double) * n);
  hostEffabs  = (double*)malloc(sizeof(double) * n);
  
  cudaMemcpy( hostPlank,  gpuPlank,   sizeof(double) * n, cudaMemcpyDeviceToHost );
  cudaMemcpy( hostRoss,   gpuRoss,    sizeof(double) * n, cudaMemcpyDeviceToHost );
  cudaMemcpy( hostEffabs, gpuEffabs,  sizeof(double) * n, cudaMemcpyDeviceToHost );
  cudaThreadSynchronize();
  
  for (int i=0; i<n; ++i) {
//    std::cout << "Temperature: " << temperature[i] << " CO2: " << co2molefrac[i] << " H2O: " << h2omolefrac[i] << " ==> CPU: " << cpuEffabs[i] << " GPU: " << hostEffabs[i] << "\n";
    if ((std::abs(hostPlank [i] - cpuPlank  [i])/cpuPlank [i]  > 1e-10) ||
        (std::abs(hostRoss  [i] - cpuRoss   [i])/cpuRoss  [i]  > 1e-10) ||
        (std::abs(hostEffabs[i] - cpuEffabs [i])/cpuEffabs[i]  > 1e-10)) status = false;
  }
  
  if (status) std::cout <<" npts = " << n << "\t" << "time CPU = " << t_cpu << ",  GPU = " << t_gpu << "  => CPU/GPU = " << t_cpu / t_gpu << std::endl;
  
  // Compare timing
  cudaFree( gpuTemperature  );
  cudaFree( gpuPlank        );
  cudaFree( gpuRoss         );
  cudaFree( gpuEffabs       );
  cudaFree( gpuCO2          );
  cudaFree( gpuH2O          );
  
  return status;
  
}

int main( int argc, char* argv[] )
{

  TestHelper status(true);
  try{
    GreyGas ggas("AbsCoeffCH4.txt");
    const size_t n = pow(33,3);
    status(time_it(ggas, n), " Check gas phase radiation properties");

  }
  catch( std::exception& err ){
    cout << err.what() << endl;
  }

}
